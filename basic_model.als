
abstract sig Condition{}
sig Critical extends Condition{}
sig Normal extends Condition{}
abstract sig Room{
	//doors: some Door,
	connections: some Room,
	people: Person
}
sig Lobby extends Room {}
sig Corridor extends Room {}
sig Waiting_Area extends Room {}
sig Surgical_Unit extends Room {}
sig Waste_Management_Unit extends Room {}
sig Nurse_Station extends Room {}
sig Consulting_Room extends Room {}
sig SickRoom extends Room {}

// or room has a set of persons
abstract sig Person{}
sig Visitor extends Person{}
sig Doctor extends Person{}
sig Nurse extends Person{}
sig Patient extends Person{
	condition: Condition
}

allRooms : run {
some t1, t2, t3, t4,t5,t6,t7,t8:Room |
{
t1 in Lobby
t2 in Corridor 
t3 in Waiting_Area
t4 in Surgical_Unit
t5 in Waste_Management_Unit 
t6 in Nurse_Station
t7 in Consulting_Room
t8 in SickRoom
} 
} for 8 but exactly 8 Room
//run for point 2.1.2.a

pred connected {
all r, r':Room | {
r in r'.connections implies r' in r.connections
}
no r:Room | r in r.connections
}

connectedRoom: run{
connected
} for 4 but exactly 3 Room

pred locations {
all r, r':Room |  {
r' != r
r.people & r'.people = none
}
}

peoples: run{
connected
locations
} for 4  but exactly 6 Room
