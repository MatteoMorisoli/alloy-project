open util/ordering[Time] as to
sig Time {}

abstract sig Room{
	connections: set Room,
	people: Person -> Time,
	lockable: one Boolean,
	locked: Boolean one -> Time
}
fact{all r:Room |all t:Time| r.lockable in False implies r.locked.t in False}
sig Lobby extends Room {}
sig Corridor extends Room {}
sig Waiting_Area extends Room {}
sig Surgical_Unit extends Room {}
sig Waste_Management_Unit extends Room {}
sig Nurse_Station extends Room {}
sig Consulting_Room extends Room {}
sig SickRoom extends Room {}
sig Outdoor extends Room {}


abstract sig Person{}
sig Visitor extends Person{}
sig Doctor extends Person{}
sig Nurse extends Person{}
sig Patient extends Person{}
sig CriticalPatient extends Person{}

abstract sig Boolean{}
sig False extends Boolean{}
sig True extends Boolean{}




//2.1.2.b  /////////////////////////////////////////////////////////////////////////////////////////
fact connected {
	all r,r':Room | {
		r.*connections = Room ///this makes sure that if there are multiple room conneections are established
		r' in r.connections implies r in r'.connections //symmetry
		r  & r.connections = none
	}
}
//2.1.2.c//////////////////////////////////////////////////////////////////////////////////////////
fact locations {
	all t:Time | all p:Person | all r:Room |{
		//no person in two rooms at same time
		p in r.people.t implies p & (Room - r).people.t= none
	}
	all t:Time | Room.people.t = Person
}
//2.1.2.d//////////////////////////////////////////////////////////////////////////////////////////
//fact ex212d{
//	all t:Time{
		//can go outdoor only from lobby
//		no o:Outdoor | o in (Room - Lobby).connections
		//patients must be inside
//		no p:Patient+CriticalPatient | p in (Outdoor.people.t) + (Waste_Management_Unit.people.t)
		//visitor can visit in sickroom only with medic for critical patients
//		all s:SickRoom | s.people.t & Visitor != none and s.people.t & CriticalPatient != none implies 
//		(s.people.t & (Nurse) != none or s.people.t & (Doctor) != none)
		//patient can not be in a surgical room or consulting  without doctor
//		all r:(Surgical_Unit) | r.people.t & Patient != none implies r.people.t & (Doctor) != none
		//no visitor in surgical or consulting or waste
//		no v:Visitor | v in ((Surgical_Unit.people.t) + (Consulting_Room.people.t))
		//visitor can be in nurse station only with nurses
//		all s:Nurse_Station | s.people.t & (Visitor+Patient) != none implies s.people.t & (Nurse) != none
//	}
//}




//2.1.2.e//////////////////////////////////////////////////////////////////////////////////////////
fact ex212e {
	// connections are already symmetric from point b
	no s:Surgical_Unit | s in Lobby.connections
	// connections are already symmetric from point b
	no c:Consulting_Room | c in Waste_Management_Unit.connections
	// connections are already symmetric from point b
	no s:Surgical_Unit | s in Waste_Management_Unit.connections
}






//2.2.1 Entering/////////////////////////////////////////////////////////////////////////////////////////////
pred enter[t: Time, p: Person, r,r':Room] {
	let t' = t.next | {
   		// precondition:
		r' != r
		r.locked.t in False && r'.locked.t in False
   		p in r.people.t
		r' in r .connections
    	// effects (postcondition):
    	r.people.t' = r.people.t - p
		r'.people.t' = r'.people.t + p
    	// frame condition:
    	(Room - r - r').people.t' = (Room - r - r').people.t
		Room.locked.t = Room.locked.t'
	}
}	 

optional2: run{
	some p: Person | some r:Room | some t:Time-to/last | {
		some r',r":Room | {
			p in r.people.t
			r != r'
			r != r"
			r' != r"
			r' in r.connections
			r" in r'.connections		
			r"  & r.connections = none
			enter[t,p,r,r']
			enter[t.next,p,r',r"]
		}	
	}
}for 3 but exactly 3 Room, exactly 1 Person, exactly 3 Time



//2.2.2 Entering///////////////////////////////////////////////////////////////////////////////////////////////////
abstract sig Operation { time: Time } 

sig Enter extends Operation{
	room1:Room,
	room2:Room,
	people : set Person
}

fact traces {
  no time.(to/last)
  all t: Time - to/last | { 
//can be time with no actions happening easier to find simple examples
    lone time.t
    exec[t] 
  }
}

fact{
	no e:Enter | e.room1 = e.room2
	all e:Enter | let t = e.time | e.people in e.room1.people.t
}

pred enter' [e:Enter] {
	let t' = e.time.next, t = e.time{
   		// precondition:
		e.room1.locked.t in False
		e.room2.locked.t in False
		e.people in e.room1.people.t
		e.people != none
		//no patients outdoor or in the waste management unit
		e.room2 in Outdoor+Waste_Management_Unit implies e.people & Patient+CriticalPatient = none
		//no visitors in surgery or consulting
		e.room2 in Surgical_Unit+Consulting_Room implies e.people & Visitor = none
		// no visit without doc or nurse
		e.room2 in SickRoom && e.room2.people.t & CriticalPatient+Patient != none && e.people & Visitor != none 
		implies e.people+e.room2.people.t' & Nurse+Doctor != none
		// critical patient can not go in surgery without a doctor
		e.room2 in Surgical_Unit && e.people & CriticalPatient != none implies e.people & Doctor != none
		//visitors in nurse station only with nurse
		e.room2 in Nurse_Station && e.people & Visitor != none implies e.people+e.room2.people.t' & Nurse != none
		
    	// effects (postcondition):
    	e.room1.people.t' = e.room1.people.t - e.people
		e.room2.people.t' = e.room2.people.t + e.people

    	// frame condition:
    	(Room - e.room1 - e.room2).people.t' = (Room - e.room1 - e.room2).people.t
		Room.locked.t = Room.locked.t'
	}
}	 

optional3: run{
	all t:Time | some p:CriticalPatient | some s:SickRoom | p in s.people.t
	some t:Time | some v:Visitor | some c:Corridor | some s:SickRoom |
 	some e:Enter | e.time = t && v in e.people && e.room1 = c && e.room2 = s && enter'[e]

	//some p:CriticalPatient | some u:Surgical_Unit | some s:SickRoom |
 	//some e:Enter | let t = e.time | let t' = t.next |  p in e.people && e.room1 = s && e.room2 = u && p in e.room2.people.t' && enter'[e]

	//some v:Visitor | some n:Nurse | some t:Time - to/last | some s:Nurse_Station | v not in s.people.t and n in s.people.t and  v in s.people.(t.next)
}for 3 but exactly 2 Room, exactly 1 Nurse, exactly 1 Visitor, exactly 1 Corridor, exactly 1 CriticalPatient, exactly 2 Time, 3 Person






//2.2.3///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//we only restric visitor to enter sickroom with critical patients,
// but the model could be initialized with a critical patient and visitor in a sickroom

critical:run{
	some v:Visitor | some p:CriticalPatient | some s:SickRoom | some t:Time|
	p+v in  s.people.t
} for 9 but exactly 1 Room, exactly 2 Person





//2.2.4/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//also critical patient has the same problem
critical2:run{
	some p:CriticalPatient | some s:Surgical_Unit | some t:Time|
	p in  s.people.t
} for 9 but exactly 1 Room, exactly 1 Person, exactly 2 Time

fact{
	let tfirst = to/first | {
		no s:SickRoom | s.people.tfirst & Visitor != none &&
		s.people.tfirst & CriticalPatient != none && s.people.tfirst & Nurse = none
		no s:Surgical_Unit | s.people.tfirst & CriticalPatient != none 
		&& s.people.tfirst & Doctor = none
	}
}



//2.2.5///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

sig Toggle extends Operation{
	d: Doctor,
	r:Room,
}


pred toggleLock [l:Toggle] {
	let t = l.time, t' = t.next{
   		// precondition:
		l.r.lockable in True
		l.d in l.r.people.t
    	// effects (postcondition):
		l.r.locked.t in True implies l.r.locked.t' in False
    	l.r.locked.t in False implies l.r.locked.t' in True
    	// frame condition:
    	Room.people.t' = Room.people.t
		(Room-l.r).locked.t = (Room-l.r).locked.t'
	}
}	 


pred exec [t: Time] {
  let curr = time.t | {
   	(one curr and curr in Enter) implies enter'[curr]
	(one curr and curr in Toggle) implies toggleLock[curr]
   	no curr implies  all r:Room | let t' = t.next | 
	r.locked.t = r.locked.t' && r.people.t = r.people.t'
  }
}


critical3:run{/// should work when last fact is commented... but it doesn't
	some r:Room | some t:Time - to/last| r.lockable in True && r.locked.t in True //&& r.people.t = none
} for 4 but exactly 1 Room, 2 Time


//check if people can move without move action
critical4:run{
	some r:Room | some t:Time - to/last| let  t' = t.next | r.people.t != r.people.t'
} for 2  but exactly 1 Enter,  exactly 2 Person, 0 Toggle, 2 Time

fact{
	no o:Outdoor | o.lockable in True
	let tfirst = to/first | {  
		no r:Room | r.locked.tfirst in True && r.people.tfirst & Doctor = none
	}
}











