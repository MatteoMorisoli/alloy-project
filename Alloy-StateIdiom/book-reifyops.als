/**
 * A local-state model where 
 * operations are reified, i.e. they're 
 * represented as objects.
 */
open util/ordering[Time] as to
sig Time {}

// This models a generic operation.
abstract sig Operation { now: Time } 

// This models an add operation with
// its parameters.
sig AddOperation extends Operation {
  book: Book,
  name: Name,
  target: Target
}

// This models a delete operation
// with its parameters.
sig DeleteOperation extends Operation {
  book: Book,
  name: Name,
  target: Target
}

// Merge operation
sig MergeOperation extends Operation {
  book1: Book,
  book2: Book
}

abstract sig Target {}
sig Name extends Target {}
sig Addr extends Target {}

sig Book {
  addr: (Name -> Target) -> Time 
}

// run { some Book } for 3 but 2 Time

pred init [t: Time] { all b: Book | no addr[b].t }

fun lookup [b: Book, n: Name, t: Time] : set Addr {
  n.^(b.addr.t) & Addr
}

pred inv [t: Time] {
  all b: Book | let addr = b.addr.t | 
   all n: Name {
    n not in n.^addr
    some addr.n => some n.addr
   }
}


// The predicate for add operation
// takes only a time (state) and 
// an AddOperation atom.
pred add [t: Time, op: AddOperation] {
  let b = op.book, n = op.name, 
  tg = op.target, t' = t.next | {
    // precondition
    n->tg not in b.addr.t
    // postcondition / effect
    b.addr.t' = b.addr.t + n->tg
    // frame conditions:
    // expresses what does not change
    // between states.
    (Book - b).addr.t' = (Book - b).addr.t
  }
}

// Same thing for delete:
// It takes a time (where it is applied)
// and a DeleteOperation atom which
// reprents its parameters.
pred delete [t: Time, op: DeleteOperation] {
  let b = op.book, n = op.name, 
  tg = op.target, t' = t.next | {
    // precondition
    n->tg in b.addr.t
    // postcondition - effect
    b.addr.t' = b.addr.t - n->tg
    // frame conditions
    (Book - b).addr.t' = (Book - b).addr.t
  }
}

pred merge[t: Time, op: MergeOperation] {
  let book1  = op.book1, book2 = op.book2,
       t' = t.next | {
    // precondition:
    book1 != book2
    // effects (postcondition):
    book1.addr.t' = book1.addr.t + book2.addr.t 
    book2.addr.t' = book1.addr.t + book2.addr.t
    // frame condition:
    (Book - book1 - book2).addr.t' = (Book - book1 - book2).addr.t
  }
} 

// This specifies which operation
// is executed at a given moment in
// time.
pred exec [t: Time] {
  let curr = now.t | {
   curr in AddOperation implies add[t,curr]
   curr in DeleteOperation implies delete[t,curr]
   curr in MergeOperation implies merge[t,curr]
  }
}

fact traces {
  init[to/first]
  no now.(to/last)
  all t: Time - to/last | { 
    inv[t]
    one now.t
    exec[t] 
  }
}

onebook: run {} for 4 but 1 Book, 5 Time
 
twobooks: run {} for 4 but exactly 2 Book, 5 Time

threebooks: run {
  // I want to construct an instance with a merge operation
  // with two books that have different addr relations.
  some t: Time | one mergeOp: MergeOperation | { 
    mergeOp.now = t
    mergeOp.book1.addr.t != mergeOp.book2.addr.t
  } 
} for 5 but exactly 3 Book, 5 Time

// What does this check verify?


