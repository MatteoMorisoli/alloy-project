/**
 * An incomplete model of an 
 * address book with local state idiom.
 */

/* The ordered Time signature represents state */
open util/ordering[Time] as to
sig Time {}

abstract sig Target {}
sig Name extends Target {}
sig Addr extends Target {}

sig Book {
  // Any 'mutable' signature has Time as the last column.
  addr: (Name -> Target) -> Time 
}

// Lookup function now takes a time, i.e. a particular
// state where to look for a name in a book.
fun lookup [b: Book, n: Name, t: Time] : set Addr {
  n.^(b.addr.t) & Addr
}

// Invariant with the local state idiom.
pred inv [t: Time] {
  all b: Book | let addr = b.addr.t | 
   all n: Name {
    n not in n.^addr
    some addr.n => some n.addr
   }
}

// Add operation with the local state idiom.
pred add [t: Time, b: Book, 
  n: Name, tg: Target] {
  // Invariant in the initial state
  inv[t]
  // precondition: a condition that
  // has to hold in order to apply
  // an operation
  n->tg not in b.addr.t
  // postcondition / effect: the condition
  // that will hold after the operation
  let t' = t.next | 
    b.addr.t' = b.addr.t + n->tg
}

// A predicate to show an example of add
// with empty pre-state.
pred showAdd[t: Time, b: Book, 
   n: Name, tg: Target] {
  // Invariant in pre-state
  inv[t]
  // empty book in the pre-state
  no b.addr.t
  // perform an add
  add[t,b,n,tg]
}

run showAdd for 4 but 1 Book, 2 Time

// Delete operation in the local state idiom.
pred delete [t: Time, b: Book, 
  n: Name, tg: Target] {
  // precondition
  n -> tg in b.addr.t
  // effect 
  let t' = t.next |
    b.addr.t' = b.addr.t - n->tg
}

pred showDelete[t: Time, b: Book, 
   n: Name, tg: Target] {
  // Invariant in pre-state
  inv[t]
  // start from a non-empty book
  some b.addr.t
  // perform a delete.
  delete[t,b,n,tg]
}

run showDelete for 4 but 1 Book, 2 Time

pred showAddDelete[t: Time, b: Book, 
   n: Name, tg: Target] {
   // start from an empty book
   no Book.addr.(to/first)
   // at time t perform an add operation
   add[t,b,n,tg]
   // at time t.next perform a delete
   // of the same entry added before.
   delete[t.next,b,n,tg]
}

// Represents a possible initial state.
pred init [t: Time] { 
  inv[t]
  all b: Book | 
  no b.addr.t 
}

// Construct a generic sequence of 
// operations.
pred traces {
  init[to/first]
  all t: Time - to/last |
  (some b: Book, n: Name, tg: Target | 
     add[t,b,n,tg]) or
  (some b: Book, n: Name, tg: Target | 
    delete[t,b,n,tg]) 
}

run traces for 4 but 1 Book, exactly 2 Addr, 5 Time
