abstract sig Room{
	connections: set Room,
	people: set Person
}
sig Lobby extends Room {}
sig Corridor extends Room {}
sig Waiting_Area extends Room {}
sig Surgical_Unit extends Room {}
sig Waste_Management_Unit extends Room {}
sig Nurse_Station extends Room {}
sig Consulting_Room extends Room {}
sig SickRoom extends Room {}
sig Outdoor extends Room {}


abstract sig Person{}
sig Visitor extends Person{}
sig Doctor extends Person{}
sig Nurse extends Person{}
sig Patient extends Person{}
sig CriticalPatient extends Person{}


//run for point 2.1.2.a//////////////////////////////////////////////////////////////////////////////////////////
//make no sense, some hospital have no surgey unit for example
ex212a: run {
	some t1, t2, t3, t4,t5,t6,t7,t8:Room | {
		t1 in Lobby
		t2 in Corridor 
		t3 in Waiting_Area
		t4 in Surgical_Unit
		t5 in Waste_Management_Unit 
		t6 in Nurse_Station
		t7 in Consulting_Room
		t8 in SickRoom
	} 
} for 8 but exactly 8 Room




//2.1.2.b  //////////////////////////////////////////////////////////////////////////////////////////
fact connected {
	all r,r':Room | {
		r.*connections = Room
		r' in r.connections implies r in r'.connections
		r  & r.connections = none
	}
}

ex212b: run{} for 4 but exactly 10 Room





//2.1.2.c//////////////////////////////////////////////////////////////////////////////////////////
fact locations {
	all p:Person | all r:Room |{
		//no person in two rooms at same time
		p in r.people implies p & (Room - r).people = none
	}
	//all room have someone in
	no r:Room | r.people = none
	//all people have a location
	Room.people = Person
}

ex212c: run{} for 3  but exactly 6 Room, 6 Person




//2.1.2.d//////////////////////////////////////////////////////////////////////////////////////////
fact ex212d{
	//can go outdoor only from lobby
	no o:Outdoor | o in (Room - Lobby).connections
	//patients must be inside
	no p:Patient+CriticalPatient | p in (Outdoor.people) + (Waste_Management_Unit.people)
	//visitor can visit in sickroom only with medic for critical patients
	all s:SickRoom | s.people & Visitor != none and s.people & CriticalPatient != none implies 
	(s.people & (Nurse) != none or s.people & (Doctor) != none)
	//patient can not be in a surgical room or consulting  without doctor
	all r:(Surgical_Unit) | r.people & Patient != none implies r.people & (Doctor) != none
	//no visitor in surgical or consulting or waste
	no v:Visitor | v in ((Surgical_Unit.people) + (Consulting_Room.people))
	//visitor can be in nurse station only with nurses
	all s:Nurse_Station | s.people & (Visitor+Patient) != none implies s.people & (Nurse) != none
}


ex212d: run{
	//some v:Visitor | v in SickRoom.people
	some v:Visitor | some s:SickRoom | v in s.people and s.people & CriticalPatient != none
}for 15 but exactly 1 Nurse, exactly 5 SickRoom, exactly 1 Patient, exactly 1 Corridor, exactly 1 Visitor, exactly 1 Doctor, exactly 1 Outdoor







//2.1.2.e//////////////////////////////////////////////////////////////////////////////////////////

//Optional. Identify a couple of other reasonable restrictions for room connections, and model them in Alloy.//////////
//no consulting next to waste
//no waste next to surgery

fact ex212e {
	// connections are already symmetric from point b
	no s:Surgical_Unit | s in Lobby.connections
	// connections are already symmetric from point b
	no c:Consulting_Room | c in Waste_Management_Unit.connections
	// connections are already symmetric from point b
	no s:Surgical_Unit | s in Waste_Management_Unit.connections
}

restrict2: run{}for 8 but exactly 4 Surgical_Unit, exactly 1 Corridor, exactly 3 Lobby

optional1: run{}for 4 but exactly 1 Surgical_Unit, exactly 1 Consulting_Room, exactly 1 Waste_Management_Unit, exactly 1 Corridor







